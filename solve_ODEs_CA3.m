function solve_ODEs_CA3()
%% Documentation for solve_ODEs_CA3.m
% This function uses ode45 to solves a system of ODEs defined by the nested
% function OED_CA3:
%
% $\frac{dT}{dV} = \frac{U_a(T_a-T)+(-r_{1A})(-\Delta H_{Rx1A})+(-r_{2A})(-\Delta H_{Rx2A})}{F_AC_{P_A}+F_BC_{P_B}+F_CC_{P_C}}$
%
% $\frac{dF_A}{dV} = r_A$ 
%
% $\frac{dF_B}{dV} = r_B$
%
% $\frac{dF_C}{dV} = r_C$
%
% that describe the molar flow rates of species A, B and C in mol/s and 
% temperature in K in a non-isothermal plug-flow reactor. The reactions are
% at steady-state but vary spatially along the volume of the reactor, hence
% V is the independent variable instead of time.
%
% $Reaction 1:\    \mathrm{A}\rightarrow \mathrm{B}\qquad  -r_{1A}=k_{1A}C_A$
%
% $Reaction 2:\    \mathrm{2A}\rightarrow \mathrm{C}\qquad -r_{2A}=k_{2A}C_A^2$
%
% Pure A is fed at a rate of 100 mol/s, a temperature of 323K, and a
% concentration of 0.1 mol/dm^3. 
%% Input & Output
% Input: 
%   V           volume,  dm^3
%
%   F_and_T     vector of dependent variables, F_i and T, i = A, B, C
%
% Output:
%   plot two figures T vs. V and F_i vs. V for i = A, B, C.
%% Author:
% Jianan Zhao
%
% Oklahoma State University, School of Chemical Engineering
%
% jianan.zhao@okstate.edu
%% Parameters & variables
% deltaH    energy per mole, J/mol
% C_p       heat capacity, J/(mol ?)
% Ua                        J/(m^3 s ?)
% Ta        temperature,    K
% k_1A      reaction rate,  1/s
% k_2A      reaction rate,  dm^3/(mol s)
% initial values
T0 = 423;       % initial temperature, K
F_A0 = 100;     % initial flow rate of A, mol/s
F_B0 = 0;       % initial flow rate of B, mol/s
F_C0 = 0;       % initial flow rate of C, mol/s
F_and_T0 = [F_A0,F_B0,F_C0,T0]; 

%% Define the system of ODEs in nested function ODE_CA3
    function output = ODEs_CA3(V,F_and_T)
        % set values for parameters & variables
        deltaH_Rx1A = -20000;    % J/(mol of A reacted in reaction 1)
        deltaH_Rx2A = -60000;    % J/(mol of A reacted in reaction 2)
        CpA = 90;
        CpB = 90;
        CpC = 180;
        C_T0 = 0.1;
        Ua = 4000;
        Ta = 100 + 273;
        E1_R = 4000;
        E2_R = 9000;
        T = F_and_T(4);
                
        k1A = 10*exp(E1_R*(1/300 - 1/T));
        k2A = 0.09*exp(E2_R*(1/300 - 1/T));
        
        F_A = F_and_T(1);
        F_B = F_and_T(2);
        F_C = F_and_T(3);
        F_T = F_A + F_B + F_C;
        % stoichiometry
        C_A = C_T0*(F_A/F_T)*(T0/T);
        C_B = C_T0*(F_B/F_T)*(T0/T);
        C_C = C_T0*(F_C/F_T)*(T0/T);
        
        % relative rates
        r1A = -k1A*C_A;
        r2A = -k2A*C_A*C_A;
        r1B = -r1A;
        r2C = -0.5*r2A;
        % net rates
        rA =  r1A + r2A; 
        rB = r1B;
        rC = r2C;
        % solutions
        % energy balance
        dT_dV = (Ua*(Ta - T) + r1A*deltaH_Rx1A + r2A*deltaH_Rx2A)/(F_A*CpA + F_B*CpB + F_C*CpC);
        % model balance
        dFA_dV = rA;
        dFB_dV = rB;
        dFC_dV = rC;
        % pack the derivatives into the output vector
        output = [dFA_dV,dFB_dV,dFC_dV,dT_dV]';
    end
%% Use ode45 to solve the system of ODEs defined by ODEs_CA3
Vspan = 0:.01:1;
[V,M] = ode45(@(V,F_and_T)ODEs_CA3(V,F_and_T), Vspan , F_and_T0);

Temp = M(:,4);
FA = M(:,1);
FB = M(:,2);
FC = M(:,3);
%% Plot results
figure
plot(V,Temp)
ylabel('T(K)')
xlabel('V(dm^3)')
title('Temperature profile')
grid on

figure
plot(V,FA,'ro')
hold on
plot(V,FB,'b--')
plot(V,FC,'g')
hold off
legend('F_A','F_B','F_C')
ylabel('F_i(mol/s)')
xlabel('V(dm^3)')
title('Profile of molar flow rate F_A, F_B and F_C')
grid on
end