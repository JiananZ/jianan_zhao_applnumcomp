import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import math

def param_estim_4lump():
    '''
%% Documentation for param_estim_4lump.m
% This function uses lscurvefit to estimate parameters of a system of ODEs defined by 
% functions ODEs, describing the conversion of heavy petroleun refining intermediates into
% more valuable lighter products via catalytic reactions.
% The three-lump model involves three subgroups: VGO (y1), gasoline (y2), gas(y3), and
% coke (y4). The equations that describle the three-lump model are:
%
% $\frac{dy_1}{dt}=-(k_{12} + k_{13} + k_{14})y_1^2$
%
% $\frac{dy_2}{dt}=k_{12}y_1^2-k_{23}y_2-k_{24}y_2$
%
% $\frac{dy_3}{dt}=k_{13}y_1^2+k_{23}y_2$
%
% $\frac{dy_4}{dt}=k_{14}y_1^2+k_{24}y_2$
%
% There are five parameters in the four-lump model: k12, k13, k14, k23 and k24. $y_i$ denotes 
% weight fraction of lump $i$. Conversion is defined as $1 - y_i$
%
%% Input & Output
% Input: 
%   N/A
%
% Output:
%   plot two figures $y_i$ vs. V and $y_i$ vs. conversion for i = 1, 2, 3.
%% Author:
% Jianan Zhao
%
% Oklahoma State University, School of Chemical Engineering
%
% jianan.zhao@okstate.edu
%% Parameters & variables
% $k_{12}, k_{13}, k_{14}, k_{23}, k_{24}$     parameters
%
% y     weight fraction
%
% t     time, h
    '''
    # Literature data for four-lump model
    tdata = np.array([1/60, 1/30, 1/20, 1/10]) #time
    ydata = np.array([[0.5074, 0.3796, 0.2882, 0.1762],
                      [0.3767, 0.4385, 0.4865, 0.5416],
                      [0.0885, 0.136,  0.1681, 0.2108],
                      [0.0274, 0.0459, 0.0572, 0.0714]]) #y1; y2, y3 at each t value

    # Guesses for parameters
    params_guess = np.array([48, 12, 4, 1, 1])
    
    #Define ODEs
    def ODEs(y,t,params):
        k_12 = params[0]
        k_13 = params[1]
        k_14 = params[2]
        k_23 = params[3]
        k_24 = params[4]
        dy1_dt = -(k_12 + k_13 + k_14)*y[0]**2
        dy2_dt = k_12*y[0]**2 - k_23*y[1] - k_24*y[1]
        dy3_dt = k_13*y[0]**2 + k_23*y[1]
        dy4_dt = k_14*y[0]**2 + k_24*y[1]
        return [dy1_dt, dy2_dt, dy3_dt, dy4_dt]    
     
    # Solve ODEs
    # Uses current params values and xdata as the final point in the tspan for the ODE solver
    def ODEmodel(tdata,*params):
        # Initial conditions for ODEs
        y0 = np.array([1, 0, 0, 0])
        y_output = np.zeros((tdata.size, 4))
        for i in np.arange(1,len(tdata)):
            t_inc = 0.005
            tspan = np.arange(0.005, tdata[i] + t_inc, t_inc)
            y_calc = odeint(ODEs,y0,tspan,args=(params,))
            y_output[i,:] = y_calc[-1,:]
        y_output = np.transpose(y_output)
        y_output = np.ravel(y_output)
        return y_output

    #Estimate parameters
    params, pcov = curve_fit(ODEmodel,tdata,np.ravel(ydata),p0=params_guess)

    # Print paramters estimation
    print('k_12 = params(1) = ', params[0])
    print('k_13 = params(2) = ', params[1])
    print('k_14 = params(3) = ', params[2])
    print('k_23 = params(4) = ', params[3])
    print('k_24 = params(5) = ', params[4])
    
    # Plot
    plt.plot(tdata,ydata[0,:],'rx', label='VGO')
    plt.plot(tdata,ydata[1,:],'b+', label='Gasoline')
    plt.plot(tdata,ydata[2,:],'go', label='Gas')
    plt.plot(tdata,ydata[3,:],'c*', label='Coke')
    t = np.arange(0.002, 1.002, 0.002)  # time span from 0.002 h to 1 h with interval 0.002 h
    y_calc = ODEmodel(t, *params)
    y_calc = np.reshape(y_calc,(4,t.size))
    plt.plot(t,y_calc[0,:],'r-', label='VGO fit')
    plt.plot(t,y_calc[1,:],'b-', label='Gasoline fit')
    plt.plot(t,y_calc[2,:],'g-', label='Gas fit')
    plt.plot(t,y_calc[3,:],'c-', label='Coke fit')
    plt.axis([0,1.05,0,1])
    plt.xlabel('time t [h]')
    plt.ylabel('y_i, wt fraction')
    # title('Data and fitted curve solution for three-lump model, y_i vs t')
    plt.grid()
    plt.show()
    
    conv = 1 - ydata[0,:]
    conv_cal = 1 - y_calc[0,:]
    plt.plot(conv,ydata[0,:],'rx', label='VGO')
    plt.plot(conv,ydata[1,:],'b+', label='Gasoline')
    plt.plot(conv,ydata[2,:],'go', label='Gas')
    plt.plot(conv,ydata[3,:],'c*', label='Coke')
    plt.plot(conv_cal,y_calc[0,:],'r-', label='VGO fit')
    plt.plot(conv_cal,y_calc[1,:],'b-', label='Gasoline fit')
    plt.plot(conv_cal,y_calc[2,:],'g-', label='Gas fit')
    plt.plot(conv_cal,y_calc[3,:],'c-', label='Coke fit')
    plt.axis([0,1,0,1])
    plt.xlabel('convertion, wt fraction')
    plt.ylabel('y_i, wt fraction')
    # title('Data and fitted curve solution for three-lump model, y_i vs conversion')
    plt.grid()
    plt.show()
param_estim_4lump() 
