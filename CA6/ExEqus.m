function z = ExEqus(z,rhs)
% Input: t and y are the independent and dependent variable values
%

% explicit equations
% rhs of explicit equation definition
for i = 1:length(rhs)
    str3 = cell2mat(rhs(i));
    z(i) = eval(str3);
end

end