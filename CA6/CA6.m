function varargout = CA6(varargin)
%CA6 MATLAB code file for CA6.fig
%      CA6, by itself, creates a new CA6 or raises the existing
%      singleton*.
%
%      H = CA6 returns the handle to a new CA6 or the handle to
%      the existing singleton*.
%
%      CA6('Property','Value',...) creates a new CA6 using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to CA6_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      CA6('CALLBACK') and CA6('CALLBACK',hObject,...) call the
%      local function named CALLBACK in CA6.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CA6

% Last Modified by GUIDE v2.5 12-Dec-2017 12:23:23

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CA6_OpeningFcn, ...
                   'gui_OutputFcn',  @CA6_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CA6 is made visible.
function CA6_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)


% Choose default command line output for CA6
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes CA6 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = CA6_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in define.
function define_Callback(hObject, eventdata, handles)
% hObject    handle to define (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% input dialouge for number of ODEs
Eqs_Setup;
uiwait;
load('eq_setup.mat','input','explicit');
numodes = input;

% Define ODEs
if input == -1 && explicit == -1 % canceled
    disp('Equation setup canceled')
elseif input == 0 && explicit == 0 % no input
    disp('No input!')
else % ODE correctly defined
if numodes > 0
    % initialize arries 
    Y = strings(numodes,1);
    Y0 = zeros(numodes,1);
    rhs = strings(numodes,1);
    ode_expression = strings(numodes,1);
    initial = strings(numodes,1);
    % define ODE
    for i = 1:numodes
        DefineODEs;
        uiwait;
        load('ode_params.mat','str1','str2','str3','y0','ODE_empty');
        
        if ODE_empty == -1
            disp('Define ODE canceled')
            uicontrol(hObject)
            return
        elseif ODE_empty == 1
            errordlg('Not enough inputs!','Invalid Input','modal')
            uicontrol(hObject)
            return 
        else       
            % determine if input independent variable is legal
            m = str2;
            if strcmp(m,str1) == 1
                errordlg('You must enter distinct charactors for independent and dependent variables','Invalid Input','modal')
                uicontrol(hObject)
                return
            end
            if i == 1 % record 1st input independent variable
                X = m;
            % determine if following input independent variable same as 1st
            elseif strcmp(X, m) ~= 1
                errordlg('You must enter the same charactor for independent variable','Invalid Input','modal')
                uicontrol(hObject)
                return
            end
            % store inputs in array
            Y(i) = str1;
            Y0(i) = y0;
            rhs(i) = str3;
            % for listbox to show inputs
            ode_expression(i,1) = strcat('d',str1,'/','d',str2,' = ',str3);
            initial(i,1) = strcat(str1,'_0=',string(y0));
            
        end
    end
    
    % Define integral limitations
    if ODE_empty == 0
        DefineIntLimt;
        uiwait;
        load('ode_params.mat','xmin','xmax');
    end
    
    % Define explicit equations
    if explicit == 1
        DefineExEqus;
        uiwait;
        load('explicit_eqs.mat','explicitEqs');
        % list input explicit equations
        set(handles.listbox3,'String',explicitEqs);
    else
        explicitEqs = '';
    end
    
    if  ODE_empty == 0
        % list input ODEs and initial values
        set(handles.listbox1,'String',ode_expression);
        set(handles.listbox2,'String',initial);
        % save all inputs into file
        save('ode_params.mat','Y','X','Y0','rhs','xmin','xmax','explicitEqs');
    end
end
end

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in calculate.
function calculate_Callback(hObject, eventdata, handles)
% hObject    handle to calculate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if exist ('ode_params.mat', 'file')
    % set step size
    prompt = {'Enter step size:'};
    dlg_title = 'Enter step size:';
    num_lines = 1;
    defaultans = {'1e-4'};
    xstep = str2double(inputdlg(prompt,dlg_title,num_lines,defaultans));
    % load input data
    load('ode_params.mat','Y','X','Y0','rhs','xmax','xmin','explicitEqs');
    Xspan = xmin:xstep:xmax;    

    % calculate system of equations
    [T,M] = ode45(@(t,y)ODEs(t,y,X,Y,rhs,explicitEqs),Xspan,Y0);
    disp('calculation complete')
    save('ode_rsls.mat','T','M');
    msgbox('Calculation completed!');
else 
    warningMessage = sprintf('No input data!');
    uiwait(msgbox(warningMessage));
end

% --- Executes on button press in plot_results.
function plot_results_Callback(hObject, eventdata, handles)
% hObject    handle to plot_results (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if exist ('ode_params.mat', 'file') && exist ('ode_rsls.mat', 'file')
    VariablePlot;
    uiwait;
    load('ode_params.mat','Y','X')
    load('ode_rsls.mat','T','M')
    
    if exist ('VariablePlot.mat', 'file')
        load('VariablePlot.mat','selectedVariable','p')
        if p == 1
            for i = 1:length(selectedVariable)
                if i == 1
                    H = M(:,selectedVariable(i));
                    YLegend = Y(selectedVariable(i));
                else
                    H = [H M(:,selectedVariable(i))];
                    YLegend = [YLegend Y(selectedVariable(i))];
                end
            end
            % generate plot 
            axes(handles.axes1)
            p = plot(T,H);
            grid on
            % generate x axis label
            xlabel(strcat({'Independent Variable - '},X)) % x axis label
            % generate y axis label
            for j = 1:length(YLegend)
                if j == 1
                    depVariableLabel = YLegend(j);
                else
                    depVariableLabel = strcat(depVariableLabel,', ',YLegend(j));
                end
            end
            ylabel(strcat({'Dependent Variables - '},depVariableLabel)) % y axis label
            title('')
            % dependent variables legend
            leg = legend(YLegend);
            set(leg,'location','best')
        else
            disp('plot canceled')
        end
    end
else 
    warningMessage = sprintf('No data!');
    uiwait(msgbox(warningMessage));
end

% --- Executes during object deletion, before destroying properties.
function define_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to define (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in save_plot.
function save_plot_Callback(hObject, eventdata, handles)
% hObject    handle to save_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

ax = handles.axes1;
figure_handle = isolate_axes(ax);
% save plot data as imageArray
figureData = export_fig('Results_Fig.png'); 
% prompt a window to allow user to specify a filename and target directory for the plot
startingFolder = userpath;
defaultFileName = fullfile(startingFolder, '*.png');
[baseFileName, folder] = uiputfile(defaultFileName, 'Save plot');

if baseFileName == 0
	% User clicked the Cancel button.
    disp('save plot canceled')
	return;
else
    fileDirectory = fullfile(folder, baseFileName)
    imwrite(figureData, fileDirectory);
    msgbox('Done!');
end


% --- Executes on button press in export2csv.
function export2csv_Callback(hObject, eventdata, handles)
% hObject    handle to export2csv (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% save calculation results as .csv file
load('ode_rsls.mat','T','M');
data = horzcat(T,M);
[file,path] = uiputfile('*.csv','Save results data');
dlmwrite([path,file],data);
msgbox('Done!');

% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox2.
function listbox2_Callback(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox2


% --- Executes during object creation, after setting all properties.
function listbox2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox3.
function listbox3_Callback(hObject, eventdata, handles)
% hObject    handle to listbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox3


% --- Executes during object creation, after setting all properties.
function listbox3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in reset.
function reset_Callback(hObject, eventdata, handles)
% hObject    handle to reset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% clear editbox
set(handles.listbox1,'String','');
set(handles.listbox2,'String','');
set(handles.listbox3,'String','');

% clear axes1
axes(handles.axes1);
cla reset; 

% delete files
delete ode_rsls.mat
delete ode_params.mat
delete Results_Fig.png
delete explicit_eqs.mat
delete eq_setup.mat
delete VariablePlot.mat
delete odes_results.csv


% --- Executes on button press in edit_explicit_equations.
function edit_explicit_equations_Callback(hObject, eventdata, handles)
% hObject    handle to edit_explicit_equations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Define explicit equations
if exist ('ode_params.mat', 'file')
    load('ode_params.mat','Y','X','Y0','rhs','xmin','xmax','explicitEqs');
    DefineExEqus;
    uiwait;
    load('explicit_eqs.mat','explicitEqs');

    save('ode_params.mat','Y','X','Y0','rhs','xmin','xmax','explicitEqs');
    % list input explicit equations
    set(handles.listbox3,'String',explicitEqs);
else
    warningMessage = sprintf('Error!');
    uiwait(msgbox(warningMessage));
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);

function figure1_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);
