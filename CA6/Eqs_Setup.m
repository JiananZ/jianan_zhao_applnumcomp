function varargout = Eqs_Setup(varargin)
% EQS_SETUP MATLAB code for Eqs_Setup.fig
%      EQS_SETUP, by itself, creates a new EQS_SETUP or raises the existing
%      singleton*.
%
%      H = EQS_SETUP returns the handle to a new EQS_SETUP or the handle to
%      the existing singleton*.
%
%      EQS_SETUP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EQS_SETUP.M with the given input arguments.
%
%      EQS_SETUP('Property','Value',...) creates a new EQS_SETUP or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Eqs_Setup_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Eqs_Setup_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Eqs_Setup

% Last Modified by GUIDE v2.5 08-Dec-2017 15:36:06

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Eqs_Setup_OpeningFcn, ...
                   'gui_OutputFcn',  @Eqs_Setup_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Eqs_Setup is made visible.
function Eqs_Setup_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Eqs_Setup (see VARARGIN)

% Choose default command line output for Eqs_Setup
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Eqs_Setup wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Eqs_Setup_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function numodes_Callback(hObject, eventdata, handles)
% hObject    handle to numodes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of numodes as text
%        str2double(get(hObject,'String')) returns contents of numodes as a double


% --- Executes during object creation, after setting all properties.
function numodes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to numodes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in clear.
function clear_Callback(hObject, eventdata, handles)
% hObject    handle to clear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.numodes,'String','');

% --- Executes on button press in OK.
function OK_Callback(hObject, eventdata, handles)
% hObject    handle to OK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

input = str2double(get(handles.numodes,'String'));
if isnan(input)
    errordlg('You must enter a numeric value','Invalid Input','modal')
    uicontrol(hObject)
    return
elseif rem(input,1)~=0 || input < 0
    errordlg('You must enter a positive integer!','Invalid Input','modal')
    input = 0;
    explicit = 0;
    save('eq_setup.mat','input','explicit');
    uicontrol(hObject)
    return 
else
    explicit = handles.radiobutton1.Value;
    % class(explicit)
    save('eq_setup.mat','input','explicit');
end

close(Eqs_Setup);

% --- Executes on button press in cancel.
function cancel_Callback(hObject, eventdata, handles)
% hObject    handle to cancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

input = -1;
explicit = -1;
save('eq_setup.mat','input','explicit');
delete(handles.figure1)


% --- Executes on button press in radiobutton1.
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1




