function varargout = DefineODEs(varargin)
% DEFINEODES MATLAB code for DefineODEs.fig
%      DEFINEODES, by itself, creates a new DEFINEODES or raises the existing
%      singleton*.
%
%      H = DEFINEODES returns the handle to a new DEFINEODES or the handle to
%      the existing singleton*.
%
%      DEFINEODES('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DEFINEODES.M with the given input arguments.
%
%      DEFINEODES('Property','Value',...) creates a new DEFINEODES or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before DefineODEs_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to DefineODEs_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help DefineODEs

% Last Modified by GUIDE v2.5 29-Nov-2017 13:34:46

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @DefineODEs_OpeningFcn, ...
                   'gui_OutputFcn',  @DefineODEs_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before DefineODEs is made visible.
function DefineODEs_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DefineODEs (see VARARGIN)

% Choose default command line output for DefineODEs
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes DefineODEs wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = DefineODEs_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
%equ=get(handles.edit1,'Sring');


function depvar_Callback(hObject, eventdata, handles)
% hObject    handle to depvar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of depvar as text
%        str2double(get(hObject,'String')) returns contents of depvar as a double


% --- Executes during object creation, after setting all properties.
function depvar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to depvar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function indepvar_Callback(hObject, eventdata, handles)
% hObject    handle to indepvar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of indepvar as text
%        str2double(get(hObject,'String')) returns contents of indepvar as a double


% --- Executes during object creation, after setting all properties.
function indepvar_CreateFcn(hObject, eventdata, handles)
% hObject    handle to indepvar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function rhs_Callback(hObject, eventdata, handles)
% hObject    handle to rhs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rhs as text
%        str2double(get(hObject,'String')) returns contents of rhs as a double


% --- Executes during object creation, after setting all properties.
function rhs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rhs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function initval_Callback(hObject, eventdata, handles)
% hObject    handle to initval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of initval as text
%        str2double(get(hObject,'String')) returns contents of initval as a double


% --- Executes during object creation, after setting all properties.
function initval_CreateFcn(hObject, eventdata, handles)
% hObject    handle to initval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in clear.
function clear_Callback(hObject, eventdata, handles)
% hObject    handle to clear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.depvar,'String','');
set(handles.indepvar,'String','');
set(handles.initval,'String','');
set(handles.rhs,'String','');


% --- Executes on button press in done.
function done_Callback(hObject, eventdata, handles)
% hObject    handle to done (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% get inputs from user
str1 = get(handles.depvar,'String'); % get dependent variable
str2 = get(handles.indepvar,'String'); % get independent variable
str3 = get(handles.rhs,'String'); % get righ hand side equation
y0 = str2double(get(handles.initval,'String')); % get initial condition

% determine if the inputs are legal
TF1 = isstrprop(str1,'digit');
TF2 = isstrprop(str2,'digit');
if TF1 == 1 | TF2 == 1 | isnan(y0)
    errordlg('You must enter a charactor instead of number for variables and a number for initial value','Invalid Input','modal')
    uicontrol(hObject)
    return
elseif isempty(str1) == 1 | isempty(str2) == 1
    ODE_empty = 1; % empty input
    save('ode_params.mat','str1','str2','str3','y0','ODE_empty');
else
    ODE_empty = 0; % inputs are not empty
    save('ode_params.mat','str1','str2','str3','y0','ODE_empty');
end
    
close(DefineODEs);

% --- Executes on button press in cancel.
function cancel_Callback(hObject, eventdata, handles)
% hObject    handle to cancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get the current position of the GUI from the handles structure
% to pass to the modal dialog.

str1 = '';
str2 = '';
str3 = '';
y0 = 0;
ODE_empty = -1;
save('ode_params.mat','str1','str2','str3','y0','ODE_empty');
delete(handles.figure1)
