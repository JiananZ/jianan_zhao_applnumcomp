y0 = 0;    
function output = ODE_CA3(t,y)
        dy_dt = 1/y;
        output = [dy_dt]';
end
%% Use ode45 to solve the system of ODEs defined by ODE_CA3

tspan = 0:.01:1;
[V,M] = ode45(@(t,y)ODE_CA3(t,y), tspan , y0);