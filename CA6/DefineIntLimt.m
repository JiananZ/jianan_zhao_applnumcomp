function varargout = DefineIntLimt(varargin)
% DEFINEINTLIMT MATLAB code for DefineIntLimt.fig
%      DEFINEINTLIMT, by itself, creates a new DEFINEINTLIMT or raises the existing
%      singleton*.
%
%      H = DEFINEINTLIMT returns the handle to a new DEFINEINTLIMT or the handle to
%      the existing singleton*.
%
%      DEFINEINTLIMT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DEFINEINTLIMT.M with the given input arguments.
%
%      DEFINEINTLIMT('Property','Value',...) creates a new DEFINEINTLIMT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before DefineIntLimt_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to DefineIntLimt_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help DefineIntLimt

% Last Modified by GUIDE v2.5 01-Dec-2017 16:47:38

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @DefineIntLimt_OpeningFcn, ...
                   'gui_OutputFcn',  @DefineIntLimt_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before DefineIntLimt is made visible.
function DefineIntLimt_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DefineIntLimt (see VARARGIN)

% Choose default command line output for DefineIntLimt
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes DefineIntLimt wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = DefineIntLimt_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function uplimit_Callback(hObject, eventdata, handles)
% hObject    handle to uplimit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of uplimit as text
%        str2double(get(hObject,'String')) returns contents of uplimit as a double


% --- Executes during object creation, after setting all properties.
function uplimit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uplimit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lowlimit_Callback(hObject, eventdata, handles)
% hObject    handle to lowlimit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of lowlimit as text
%        str2double(get(hObject,'String')) returns contents of lowlimit as a double


% --- Executes during object creation, after setting all properties.
function lowlimit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lowlimit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in clear.
function clear_Callback(hObject, eventdata, handles)
% hObject    handle to clear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.lowlimit,'String','');
set(handles.uplimit,'String','');

% --- Executes on button press in done.
function done_Callback(hObject, eventdata, handles)
% hObject    handle to done (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

xmax = str2double(get(handles.uplimit,'String'));
xmin = str2double(get(handles.lowlimit,'String'));

% determine if 
if isnan(xmin) ~= 0 || isnan(xmax) ~= 0
    errordlg('You must enter a numeric value','Invalid Input','modal')
    uicontrol(hObject)
    return
elseif xmin > xmax || xmin == xmax
    errordlg('The upper limit must be greater than the lower limit','modal')
    uicontrol(hObject)
    return
else
    save('ode_params.mat','xmin','xmax');
end

close(DefineIntLimt);

% --- Executes on button press in cancel.
function cancel_Callback(hObject, eventdata, handles)
% hObject    handle to cancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

delete(handles.figure1)
