function varargout = DefineExEqus(varargin)
% DEFINEEXEQUS MATLAB code for DefineExEqus.fig
%      DEFINEEXEQUS, by itself, creates a new DEFINEEXEQUS or raises the existing
%      singleton*.
%
%      H = DEFINEEXEQUS returns the handle to a new DEFINEEXEQUS or the handle to
%      the existing singleton*.
%
%      DEFINEEXEQUS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DEFINEEXEQUS.M with the given input arguments.
%
%      DEFINEEXEQUS('Property','Value',...) creates a new DEFINEEXEQUS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before DefineExEqus_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to DefineExEqus_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help DefineExEqus

% Last Modified by GUIDE v2.5 12-Dec-2017 16:05:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @DefineExEqus_OpeningFcn, ...
                   'gui_OutputFcn',  @DefineExEqus_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before DefineExEqus is made visible.
function DefineExEqus_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DefineExEqus (see VARARGIN)

if exist ('ode_params.mat', 'file')
    load('ode_params.mat')
    if ~isempty(who('-file', 'ode_params.mat', 'explicitEqs'))
        disp('a')
        class(explicitEqs)
        set(handles.editbox1,'String',explicitEqs);
    end
end

% Choose default command line output for DefineExEqus
handles.output = hObject;

set(handles.editbox1,'String','');

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes DefineExEqus wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = DefineExEqus_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function editbox1_Callback(hObject, eventdata, handles)
% hObject    handle to editbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editbox1 as text
%        str2double(get(hObject,'String')) returns contents of editbox1 as a double


% --- Executes during object creation, after setting all properties.
function editbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in clear.
function clear_Callback(hObject, eventdata, handles)
% hObject    handle to clear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.editbox1,'String','');

% --- Executes on button press in done.
function done_Callback(hObject, eventdata, handles)
% hObject    handle to done (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
explicitEqs = get(handles.editbox1,'String');
if isempty(explicitEqs) == 1
    errordlg('No input!','Invalid Input','modal')
    uicontrol(hObject)
    return
else
    save('explicit_eqs.mat','explicitEqs');
end
close(DefineExEqus);

% --- Executes on button press in cancel.
function cancel_Callback(hObject, eventdata, handles)
% hObject    handle to cancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

delete(handles.figure1)
