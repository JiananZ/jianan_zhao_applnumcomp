function varargout = VariablePlot(varargin)
% VARIABLEPLOT MATLAB code for VariablePlot.fig
%      VARIABLEPLOT, by itself, creates a new VARIABLEPLOT or raises the existing
%      singleton*.
%
%      H = VARIABLEPLOT returns the handle to a new VARIABLEPLOT or the handle to
%      the existing singleton*.
%
%      VARIABLEPLOT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in VARIABLEPLOT.M with the given input arguments.
%
%      VARIABLEPLOT('Property','Value',...) creates a new VARIABLEPLOT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before VariablePlot_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to VariablePlot_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help VariablePlot

% Last Modified by GUIDE v2.5 10-Dec-2017 11:30:17

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @VariablePlot_OpeningFcn, ...
                   'gui_OutputFcn',  @VariablePlot_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before VariablePlot is made visible.
function VariablePlot_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to VariablePlot (see VARARGIN)

% Choose default command line output for VariablePlot
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes VariablePlot wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = VariablePlot_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

load('ode_params.mat','Y')
set(handles.listbox1,'String',Y);


% --- Executes on button press in done.
function done_Callback(hObject, eventdata, handles)
% hObject    handle to done (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get the dependent variables selected in listbox1 
selectedVariable = get(handles.listbox1, 'value');
p = 1;
save('VariablePlot.mat','selectedVariable','p')
close(VariablePlot);


% --- Executes on button press in cancel.
function cancel_Callback(hObject, eventdata, handles)
% hObject    handle to cancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

selectedVariable = 0;
p = 0;
save('VariablePlot.mat','selectedVariable','p')
delete(handles.figure1)

% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
