function dy_dt = ODEs(t,y,X,Y,rhs,explicitEqs)
% Input: t and y are the independent and dependent variable values
% X, Y, and rhs are 

% independent variable
str0 = X;
eval(strcat(str0,'=t;'));

% dependent variable
for i = 1:length(Y)
    str1 = cell2mat(Y(i));
    eval(strcat(str1,'=y(i);'));
end

% explicit equations
if isempty(explicitEqs) ~= 1
    eval(explicitEqs);
end

% rhs of ODE definition
for i = 1:length(rhs)
    str3 = cell2mat(rhs(i));
    dy_dt(i) = eval(str3);
end

dy_dt = dy_dt';
end
