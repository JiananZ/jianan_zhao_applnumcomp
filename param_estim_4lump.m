 
function param_estim_4lump()
%% Documentation for param_estim_4lump.m
% This function uses lscurvefit to estimate parameters of a system of ODEs defined by 
% functions ODEs, describing the conversion of heavy petroleun refining intermediates into
% more valuable lighter products via catalytic reactions.
% The three-lump model involves three subgroups: VGO (y1), gasoline (y2), gas(y3), and
% coke (y4). The equations that describle the three-lump model are:
%
% $\frac{dy_1}{dt}=-(k_{12} + k_{13} + k_{14})y_1^2$
%
% $\frac{dy_2}{dt}=k_{12}y_1^2-k_{23}y_2-k_{24}y_2$
%
% $\frac{dy_3}{dt}=k_{13}y_1^2+k_{23}y_2$
%
% $\frac{dy_4}{dt}=k_{14}y_1^2+k_{24}y_2$
%
% There are five parameters in the four-lump model: k12, k13, k14, k23 and k24. $y_i$ denotes 
% weight fraction of lump $i$. Conversion is defined as $1 - y_i$
%
%% Input & Output
% Input: 
%   N/A
%
% Output:
%   plot two figures $y_i$ vs. V and $y_i$ vs. conversion for i = 1, 2, 3.
%% Author:
% Jianan Zhao
%
% Oklahoma State University, School of Chemical Engineering
%
% jianan.zhao@okstate.edu
%% Parameters & variables
% $k_{12}, k_{13}, k_{14}, k_{23}, k_{24}$     parameters
%
% y     weight fraction
%
% t     time, h
%% Literature data for four-lump model
tdata = [1/60, 1/30, 1/20, 1/10]; % time
ydata = [0.5074, 0.3796, 0.2882, 0.1762; ...
         0.3767, 0.4385, 0.4865, 0.5416; ...
         0.0885, 0.136,  0.1681, 0.2108; ...
         0.0274, 0.0459, 0.0572, 0.0714]; % y1; y2; y3 at each time point

%% Guesses for parameters
k(1) = 48;
k(2) = 12;
k(3) = 4;
k(4) = 1;
k(5) = 1;
params_guess = k;

%% Initial conditions for ODEs
y0(1) = 1;
y0(2) = 0;
y0(3) = 0;
y0(4) = 0;

%% Options for lsqcurvefit
%OPTIONS = optimoptions(@lsqcurvefit,'TolX', 1e-10, ...
%    'TolFun', 1e-10,'StepTolerance',1e-10); 

%% Estimate parameters
[params,resnorm,residuals,exitflag,] = lsqcurvefit(@(params,ydata) ...
    ODEmodel(params,tdata,y0),params_guess,tdata,ydata, [], []);

%% Print paramters estimation
fprintf('k_12 = params(1) = %f\n', params(1))
fprintf('k_13 = params(2) = %f\n', params(2))
fprintf('k_14 = params(3) = %f\n', params(3))
fprintf('k_23 = params(4) = %f\n', params(4))
fprintf('k_24 = params(5) = %f\n', params(5))

%% Plot
figure(1)

plot(tdata,ydata(1,:),'rx')
hold on
plot(tdata,ydata(2,:),'b+')
plot(tdata,ydata(3,:),'go')
plot(tdata,ydata(4,:),'c*')
t = 0.002:0.002:1;          % time span from 0.002 h to 1 h with interval 0.002 h
y_calc = ODEmodel(params,t,y0);
plot(t,y_calc(1,:),'r-')
plot(t,y_calc(2,:),'b-')
plot(t,y_calc(3,:),'g-')
plot(t,y_calc(4,:),'c-')
axis([0,1.05,0,1])
xlabel('time t [h]')
ylabel('y_i, wt fraction')
legend('VGO','Gasoline','Gas','Coke','VGO fit','Gasoline fit','Gas fit','Coke fit')
title('Figure 1: Data and fitted curve solution for four-lump model, y_i vs. t')

figure(2)
conv = 1 - ydata(1,:);
conv_cal = 1 - y_calc(1,:);
plot(conv,ydata(1,:),'rx')
hold on
plot(conv,ydata(2,:),'b+')
plot(conv,ydata(3,:),'go')
plot(conv,ydata(4,:),'c*')
plot(conv_cal,y_calc(1,:),'r-')
plot(conv_cal,y_calc(2,:),'b-')
plot(conv_cal,y_calc(3,:),'g-')
plot(conv_cal,y_calc(4,:),'c-')
axis([0,1,0,1])
hold off
xlabel('convertion, wt fraction')
ylabel('y_i, wt fraction')
legend('VGO','Gasoline','Gas','Coke','VGO fit','Gasoline fit','Gas fit','Coke fit')
title('Figure 2: Data and fitted curve solution for four-lump model, y_i vs. conversion')

end

%% Define ODEs
function dy_dt = ODEs(t,y,params)
    k_12 = params(1);
    k_13 = params(2);
    k_14 = params(3);
    k_23 = params(4);
    k_24 = params(5);
    
    dy1_dt = -(k_12 + k_13 + k_14)*y(1)^2;
    dy2_dt = k_12*y(1)^2 - k_23*y(2) - k_24*y(2);
    dy3_dt = k_13*y(1)^2 + k_23*y(2);
    dy4_dt = k_14*y(1)^2 + k_24*y(2);
    dy_dt = [dy1_dt, dy2_dt, dy3_dt, dy4_dt]';
end

%% Solve ODEs
% Uses current params values and ydata as the final point in the tspan for the ODE solver
function y_output = ODEmodel(params,tdata,y0)
    for i = 1:length(tdata)
        tspan = 0.001:0.001:tdata(i);
        [~,y_calc] = ode23s(@(t,y) ODEs(t,y,params),tspan,y0);
        y_output(i,:)=y_calc(end,:);
    end
    y_output = y_output';
end