%% system_of_ODEs.m
%   Jianan Zhao -- 9/12/2017
%%   Function description
%   Define a system of ordinary differential equations,
%
% $$\frac{dC_{A}}{dt} = -k_{1}C_{A} - k_{2}C_{A}$$
% 
% $$\frac{dC_{B}}{dt} = k_{1}C_{A} - k_{3} - k_{4}C_{B}$$
%
% which describe the mass concentrations of species A and B. 
% $C_{A}$ and $C_{B}$ are concentrations of A & B, which are dependent 
% variables, time t is independent variable, and four parameters
% $k_{1}$, $k_{2}$, $k_{3}$, and $k_{4}$ are rate constant.
% C is a 1 X 2 matrix, written as $C = [C_{A}, C_{B}]$.
%%
% The output of this function is a 2 X 1 matrix, describing the
% rate of change of concentration of A & B, written as 
% $[\frac{dC_{A}}{dt}$
% $\frac{dC_{B}}{dt}]^T$

function dC_over_dt = system_of_ODEs(varargin)
%% set variables & parameters
%   if all variables & parameters are specified
if nargin == 7
    react_time = varargin{1};       % t = reaction time [h]
    concentrat_A = varargin{2};     % C_A = concentration of A [mg/L]
    concentrat_B = varargin{3};     % C_B = concentration of B [mg/L]
    rate_k1 = varargin{4};          % k_1 = rate constant  [1/h]
    rate_k2 = varargin{5};          % k_2 = rate constant  [1/h]
    rate_k3 = varargin{6};          % k_3 = rate constant  [mg/(L h)]
    rate_k4 = varargin{7};          % k_4 = rate constant  [1/h]
    
    concentrat = [concentrat_A, concentrat_B];

elseif nargin == 3 || nargin == 0
    rate_k1 = 0.15;                 % k_1 = rate constant  [1/h]
    rate_k2 = 0.6;                  % k_2 = rate constant  [1/h]
    rate_k3 = 0.1;                  % k_3 = rate constant  [mg/(L h)]
    rate_k4 = 0.2;                  % k_4 = rate constant  [1/h]
    if nargin == 3
    %   if only t and C are supplied
        react_time = varargin{1};   % t = reaction time [h]
        concentrat_A = varargin{2}; % C_A = concentration of A [mg/L]
        concentrat_B = varargin{3}; % C_B = concentration of B [mg/L]
        
        concentrat = [concentrat_A, concentrat_B];
    else
    %   if nothing is supplied
        react_time = 0;             % t = reaction time [h]
        concentrat_A = 0.625;       % C_A = concentration of A [mg/L]
        concentrat_B = 0;           % C_B = concentration of B [mg/L]

        concentrat = [concentrat_A, concentrat_B];
    end
else
    fprintf('Wrong number of inputs. Please try again. \n');
    return;
end


%% set output
%   2 X 1 matrix of differential equations evaluated at time t
%   d C / d t = [ d C_A / dt; d C_B / dt]
dC_Aover_dt = -rate_k1*concentrat_A - rate_k2*concentrat_A;
dC_Bover_dt = rate_k1*concentrat_A - rate_k3 - rate_k4*concentrat_B;
dC_over_dt = [dC_Aover_dt; dC_Bover_dt];

%% print out the output
fprintf ('At time t = %f ,\n', react_time);
fprintf ('The rate changes of concentration of A & B are: ');




