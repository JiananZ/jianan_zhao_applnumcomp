import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def param_estim_3lump():
    '''
Documentation for param_estim_3lump.m
% This function uses lscurvefit to estimate parameters of a system of ODEs defined by 
% functions ODEs, describing the conversion of heavy petroleun refining intermediates into
% more valuable lighter products via catalytic reactions.
% The three-lump model involves three subgroups: VGO (y1), gasoline (y2) and the sum of
% gas and coke (y3). The equations that describle the three-lump model are:
%
% $\frac{dy_1}{dt}=-(k_1 + k_3)y_1^2$
%
% $\frac{dy_2}{dt}=k_1y_1^2-k_2y_2$
%
% $\frac{dy_3}{dt}=k_3y_1^2+k_2y_2$
%
% There are thre parameters in the three-lump model: k1, k2, and k3. $y_i$ denotes 
% weight fraction of lump $i$. Conversion is defined as $1 - y_i$
%
%% Input & Output
% Input: 
%   N/A
%
% Output:
%   plot two figures $y_i$ vs. V and $y_i$ vs. conversion for i = 1, 2, 3.
%% Author:
% Jianan Zhao
%
% Oklahoma State University, School of Chemical Engineering
%
% jianan.zhao@okstate.edu
%% Parameters & variables
% $k_1, k_2, k_3$     parameters
%
% y     weight fraction
%
% t     time, h
    '''
    # Literature data for three-lump model
    tdata = np.array([1/60, 1/30, 1/20, 1/10]) #time
    ydata = np.array([[0.5074, 0.3796, 0.2882, 0.1762],
                      [0.3767, 0.4385, 0.4865, 0.5416],
                      [0.0885, 0.136,  0.1681, 0.2108]]) #y1; y2, y3 at each t value

    # Guesses for parameters
    params_guess = np.array([38, 2, 10])
    
    # Define ODEs
    def ODEs(y,t,params):
        k_1 = params[0]
        k_2 = params[1]
        k_3 = params[2]
        dy1_dt = -(k_1 + k_3)*y[0]*y[0]
        dy2_dt = k_1*y[0]**2 - k_2*y[1]
        dy3_dt = k_3*y[0]**2 + k_2*y[1]
        return [dy1_dt, dy2_dt, dy3_dt]
     
    # Solve ODEs
    # Uses current params values and xdata as the final point in the tspan for the ODE solver
    def ODEmodel(tdata,*params):
        # Initial conditions for ODEs
        y0 = np.array([1, 0, 0])
        y_output = np.zeros((tdata.size, 3))
        for i in np.arange(1,len(tdata)):
            t_inc = 0.005
            tspan = np.arange(0.005, tdata[i] + t_inc, t_inc)
            y_calc = odeint(ODEs,y0,tspan,args=(params,))
            y_output[i,:] = y_calc[-1,:]
        y_output = np.transpose(y_output)
        y_output = np.ravel(y_output)
        return y_output
    
    #Estimate parameters
    params, pcov = curve_fit(ODEmodel,tdata,np.ravel(ydata),p0=params_guess)
    # Print paramters estimation
    print('k_1 = params(1) = ', params[0])
    print('k_2 = params(2) = ', params[1])
    print('k_3 = params(3) = ', params[2])
    
    # Plot
    plt.plot(tdata,ydata[0,:],'rx', label='VGO')
    plt.plot(tdata,ydata[1,:],'b+', label='Gasoline')
    plt.plot(tdata,ydata[2,:],'go', label='Gas')
    t = np.arange(0.002, 1.002, 0.002)  # time span from 0.002 h to 1 h with interval 0.002 h
    y_calc = ODEmodel(t, *params)
    y_calc = np.reshape(y_calc,(3,t.size))
    
    plt.plot(t,y_calc[0,:],'r-', label='VGO fit')
    plt.plot(t,y_calc[1,:],'b-', label='Gasoline fit')
    plt.plot(t,y_calc[2,:],'g-', label='Gas fit')
    plt.axis([0,1.05,0,1])
    plt.xlabel('time t [h]')
    plt.ylabel('y_i, wt fraction')
    # title('Data and fitted curve solution for three-lump model, y_i vs t')
    plt.grid()
    plt.show()
    
    conv = 1 - ydata[0,:]
    conv_cal = 1 - y_calc[0,:]
    plt.plot(conv,ydata[0,:],'rx', label='VGO')
    plt.plot(conv,ydata[1,:],'b+', label='Gasoline')
    plt.plot(conv,ydata[2,:],'go', label='Gas')
    plt.plot(conv_cal,y_calc[0,:],'r-', label='VGO fit')
    plt.plot(conv_cal,y_calc[1,:],'b-', label='Gasoline fit')
    plt.plot(conv_cal,y_calc[2,:],'g-', label='Gas fit')
    plt.axis([0,1,0,1])
    plt.xlabel('convertion, wt fraction')
    plt.ylabel('y_i, wt fraction')
    # title('Data and fitted curve solution for three-lump model, y_i vs conversion')
    plt.grid()
    plt.show()
param_estim_3lump()