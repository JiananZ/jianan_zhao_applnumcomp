\documentclass[12pt]{article}
\usepackage{titlesec}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{indentfirst}
\usepackage{booktabs}
\titlelabel{\thetitle.\quad}
\title{\vspace{5cm}Case Study for Attached Boundary Layers with Surface Heat Transfer}
\date{\today}
\author{Jianan Zhao}
\begin{document}

\maketitle

\newpage
\tableofcontents
\listoffigures
\listoftables
\newpage
\section{PROBLEM DESCRIPTION} \label{sec:sec1}
\subsection{Experimental methods}

In this report, surface heat transfer, including both heating and cooling, of attached boundary layers has been calculated using Wilcox Companion Software: Turbulence Modeling for CFD. Ten different cases, under subsonic, supersonic and hypersonic regime, involving surface curvature and adverse pressure gradient, have been tested and compared with experimental data collected from a number of papers in the Reference section. Specifically, the predicted surface heat transfer rates are obtained from the following turbulence models:
\begin{enumerate}
  \item Wilcox k-$\omega$,
  \item Wilcox stress-$\omega$,
  \item Baldwin-Lomax,
  \item Spalart-Allmaras,
  \item Launder-Sharma.
\end{enumerate}
Among all the models, Wilcox stress-$\omega$ has the most potential in reproducing experimental data, especially in the last case of a Mach 9.22 flow over a compression corner; whereas Launder-Sharma consistently yields the largest deviations from the measured results, due partly to its poor performance in calculating laminar-turbulence transition. 
	
\subsection{Methodology}

A literature survey is first conducted in search of experiments on attached boundary layer with surface heat transfer \cite{book1}. Ten different cases from four journal publications are then selected to include a range of conditions. For each case, experimental results are manually tabulated and graphically compared with model predictions. In most of the cases, however, complete turbulent initial conditions are not specified; hence, the calculations are preformed from laminar conditions. For Launder-Sharma model, an additional calculation is required (usually using Wilcox stress-$omega$) as it cannot stably run over laminar-turbulent transitions.

\section{RESULTS} \label{sec:sec2}
\subsection{Test case one: supersonic air flow over a flat plate} \label{subsec:subsec1}
	
As mentioned in section \ref{sec:sec1}, the ratio of surface temperature to freestream temperature is 2.7 with surface temperature 100 F. Freestream Mach number is 2.5. 

\begin{equation}
	c_H = \frac{c_f}{2s},
	\label{equ:equation1}
\end{equation}
Local heat transfer coefficient $c_H$ can be computed by Eq. (\ref{equ:equation1}), where $c_f$ is the local skin friction and s is local Prandtl number.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.75 \textwidth]{figure1}
	\caption{Model outputs of heat transfer coefficient at edge of 	boundary layer vs. MIT data for supersonic flow over a flat 		plate.}
	\label{figure1}
\end{figure}

\begin{equation}
	\frac{\partial (\rho u_{i})}{\partial t} + \frac{\partial[\rho u_{i}u_{j}]}{\partial x_{j}} = -\frac{\partial p}{\partial x_{i}} + \frac{\partial \tau_{ij}}{\partial x_{j}} + \rho f_{i} 
	\label{equ:equation2}
\end{equation}

As shown in Figure \ref{figure1}, although most models predict the correct trend of heat coefficient with Re, the Wilcox k-$omega$ model-predicted heat transfer coefficients are much closer to measurements than those of any other turbulence model applied in this report. Computed from Eq. (\ref{equ:equation2}), the reason that Launder-Sharma model fails at correspondingly low Reynolds number is because the flow transfer into transition very quickly, where the model is highly inaccurate.

\begin{table}[ht]
  \centering
  \caption{Experimental data of heat transfer coefficient with Re 		for supersonic flow over a flat plate.}
  \label{tab:table1}
  \begin{tabular}{llllll}
    \toprule
    Re ($\times 10^6$) & $c_H$ & Re ($\times 10^6$) & $c_H$ & Re 		($\times 10^7$) & $c_H$\\
    \midrule

    1.27 & 0.00140 & 2.95 & 0.00121 & 1.10 &0.00097\\
    1.35 & 0.00137 & 3.30 & 0.00161 & 1.28 &0.00089\\
    1.27 & 0.00140 & 3.36 & 0.00126 & 1.65 &0.00105\\
    1.93 & 0.00162 & 3.85 & 0.00105 & 1.70 &0.00095\\
    2.48 & 0.00122 & 6.24 & 0.00110\\
    \bottomrule
  \end{tabular}
\end{table}

\subsection{Test case two: Smith-kuethe incompressible flow over a flat plate}
	Smith and Kuethe at University of Michigan tested effects of turbulence on heat transfer in two low-turbulence wind tunnels. Experimental parameters: surface temperature 20 F, freestream temperature 40 F and freestream velocity 40.1 ft/s \cite{article1}. Nusselt number at wall are computed from various models comparing with measurement.

\begin{table}[ht]
  \centering
  \caption{Various models output of heat transfer coefficient with Re for a supersonic flow.}
  \label{tab:table2}
  \begin{tabular}{llllll}
    \toprule
    \multicolumn{2}{c}{Baldwin-Lomax} & \multicolumn{2}{c}{Wilcox k-$\omega$} & \multicolumn{2}{c}{Wilcox Stress-$\omega$}\\
    \midrule
    Re($\times 10^6$) & $c_H (\times 10^{-3})$ & Re($\times 10^6$) & $c_H (\times 10^{-3})$ & Re($\times 10^6$) & $c_H (\times 10^{-3})$\\
    \midrule
    1.28 & 1.61 & 3.71 & 1.96 & 1.27 & 1.61\\
    2.06 & 1.48 & 4.72 & 2.14 & 2.09 & 1.47\\
    2.67 & 1.43 & 5.62 & 1.99 & 2.67 & 1.42\\
    \bottomrule
  \end{tabular}
\end{table}

All the models produce a reasonable prediction at high Reynolds number, except Launder-Sharma. Wilcox k-$\omega$ and Stress-$\omega$ give better approximation to the measured data than other models, however, produce wrong trend at low Reynolds number. In general, results from Baldwin-Lomax model matches the measurements best.

From the same paper of Thomann, we compute two more cases of supersonic, heated flow with constant pressure both over concave and convex surface. Freestream conditions and boundary conditions are the same. 

As mentioned in section \ref{subsec:subsec1}, none of these turbulence models give a good prediction for a supersonic flow over a heated concave plate case. Somehow, the predicted values at the wall from Wilcox Stress-$\omega$ model can match measurements extremely well except at the beginning of the curved surface. For flow over a heated convex plate case, again, all the models’ predicted values are way off from the experimental data, although Wilcox Stress-$\omega$ model gives the best approximation to measured data.

\medskip

\addcontentsline{toc}{section}{References}
\bibliographystyle{abbrv}
\bibliography{bib_Jianan_Zhao}

\end{document}
