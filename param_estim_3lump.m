function param_estim_3lump()
%% Documentation for param_estim_3lump.m
% This function uses lscurvefit to estimate parameters of a system of ODEs defined by 
% functions ODEs, describing the conversion of heavy petroleun refining intermediates into
% more valuable lighter products via catalytic reactions.
% The three-lump model involves three subgroups: VGO (y1), gasoline (y2) and the sum of
% gas and coke (y3). The equations that describle the three-lump model are:
%
% $\frac{dy_1}{dt}=-(k_1 + k_3)y_1^2$
%
% $\frac{dy_2}{dt}=k_1y_1^2-k_2y_2$
%
% $\frac{dy_3}{dt}=k_3y_1^2+k_2y_2$
%
% There are thre parameters in the three-lump model: k1, k2, and k3. $y_i$ denotes 
% weight fraction of lump $i$. Conversion is defined as $1 - y_i$
%
%% Input & Output
% Input: 
%   N/A
%
% Output:
%   plot two figures $y_i$ vs. V and $y_i$ vs. conversion for i = 1, 2, 3.
%% Author:
% Jianan Zhao
%
% Oklahoma State University, School of Chemical Engineering
%
% jianan.zhao@okstate.edu
%% Parameters & variables
% $k_1, k_2, k_3$     parameters
%
% y     weight fraction
%
% t     time, h
%% Literature data for three-lump model
tdata = [1/60, 1/30, 1/20, 1/10]; %time
ydata = [0.5074, 0.3796, 0.2882, 0.1762; ...
         0.3767, 0.4385, 0.4865, 0.5416; ...
         0.0885, 0.136,  0.1681, 0.2108]; %y1; y2, y3 at each t value

%% Guesses for parameters
k(1) = 38;
k(2) = 2;
k(3) = 10;
params_guess = k;

%% Initial conditions for ODEs
y0(1) = 1;
y0(2) = 0;
y0(3) = 0;

%% Options for lsqcurvefit
%OPTIONS = optimoptions(@lsqcurvefit,'TolX', 1e-14, ...
%    'TolFun', 1e-14,'StepTolerance',1e-14); 

%% Estimate parameters
[params,resnorm,residuals,exitflag,] = lsqcurvefit(@(params,ydata) ...
    ODEmodel(params,tdata,y0),params_guess,tdata,ydata, [], []);

%% Print paramters estimation
fprintf('k_1 = params(1) = %f\n', params(1))
fprintf('k_2 = params(2) = %f\n', params(2))
fprintf('k_3 = params(3) = %f\n', params(3))

%% Plot
figure(1)
plot(tdata,ydata(1,:),'rx')
hold on
plot(tdata,ydata(2,:),'b+')
plot(tdata,ydata(3,:),'go')
t = 0.002:0.002:1;          % time span from 0.002 h to 1 h with interval 0.002 h
y_calc = ODEmodel(params,t,y0);
plot(t,y_calc(1,:),'r-')
plot(t,y_calc(2,:),'b-')
plot(t,y_calc(3,:),'g-')
axis([0,1.05,0,1])
xlabel('time t [h]')
ylabel('y_i, wt fraction')
legend('VGO','Gasoline','Gas','VGO fit','Gasoline fit','Gas fit')
title('Data and fitted curve solution for three-lump model, y_i vs t')

figure(2)
conv = 1 - ydata(1,:);
conv_cal = 1 - y_calc(1,:);
plot(conv,ydata(1,:),'rx')
hold on
plot(conv,ydata(2,:),'b+')
plot(conv,ydata(3,:),'go')
plot(conv_cal,y_calc(1,:),'r-')
plot(conv_cal,y_calc(2,:),'b-')
plot(conv_cal,y_calc(3,:),'g-')
axis([0,1,0,1])
hold off
xlabel('convertion, wt fraction')
ylabel('y_i, wt fraction')
legend('VGO','Gasoline','Gas','VGO fit','Gasoline fit','Gas fit')
title('Data and fitted curve solution for three-lump model, y_i vs conversion')
end

%% Define ODEs
function dy_dt = ODEs(t,y,params)
    k_1 = params(1);
    k_2 = params(2);
    k_3 = params(3);
    dy1_dt = -(k_1 + k_3)*y(1)^2;
    dy2_dt = k_1*y(1)^2 - k_2*y(2);
    dy3_dt = k_3*y(1)^2 + k_2*y(2);
    dy_dt = [dy1_dt, dy2_dt, dy3_dt]';
end

%% Solve ODEs
% Uses current params values and xdata as the final point in the tspan for the ODE solver
function y_output = ODEmodel(params,tdata,y0)
    for i = 1:length(tdata)
        tspan = 0.001:0.001:tdata(i);
        [~,y_calc] = ode23s(@(t,y) ODEs(t,y,params),tspan,y0);
        y_output(i,:)=y_calc(end,:);
    end
    y_output = y_output';
end